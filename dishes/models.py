from django.db import models


class Cook(models.Model):
    first_name = models.CharField(max_length=100, blank=False, null=False)
    second_name = models.CharField(max_length=100, blank=True, null=False)
    experience = models.IntegerField(null=False)

    def __str__(self):
        return self.first_name


class Dish(models.Model):
    cook = models.ForeignKey(Cook, on_delete=models.CASCADE, related_name='dishes')
    name = models.CharField(max_length=100, blank=False, null=False)
    price = models.IntegerField(null=False)
    ingredients = models.ManyToManyField('Ingredient')

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.name
