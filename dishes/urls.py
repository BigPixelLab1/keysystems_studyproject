from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register('cook', views.CookViewSet, basename='cook')
router.register('dish', views.DishViewSet, basename='dish')
router.register('ingredient', views.DishViewSet, basename='ingredient')

urlpatterns = router.urls
