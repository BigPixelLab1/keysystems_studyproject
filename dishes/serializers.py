from rest_framework import serializers

from . import models


class CookSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cook
        fields = [
            'pk',
            'first_name',
            'second_name',
            'experience'
        ]


class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Dish
        fields = [
            'pk',
            'cook',
            'name',
            'price',
            'ingredients'
        ]


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Ingredient
        fields = [
            'pk',
            'name'
        ]
