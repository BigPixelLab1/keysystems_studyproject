from rest_framework.viewsets import ModelViewSet

from . import models, serializers


class CookViewSet(ModelViewSet):
    serializer_class = serializers.CookSerializer
    queryset = models.Cook.objects.all()


class DishViewSet(ModelViewSet):
    serializer_class = serializers.DishSerializer
    queryset = models.Dish.objects.all()


class IngredientViewSet(ModelViewSet):
    serializer_class = serializers.IngredientSerializer
    queryset = models.Ingredient.objects.all()
